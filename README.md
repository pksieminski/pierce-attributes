# Pierce Attributes

Test project for Pierce Ecom - Attributes & Options with translations

## Table of contents
* [Technologies](#technologies)
* [Running Project](#running-project)
* [Fixtures](#fixtures)
* [Tests](#tests)
* [Features](#features)

## Technologies
Project is created with:
* [Maven](https://maven.apache.org/) - Software project management tool
* [Spring](https://spring.io/projects/spring-framework) - Java application framework
* [Spring Boot](https://spring.io/projects/spring-boot) - Spring extension
* [H2 Database](https://www.h2database.com/html/main.html) - Java SQL Database
* [Docker](https://www.docker.com/) - Virtualization tool using containers
* [Docker Compose](https://docs.docker.com/compose/) - Tool for multi-container Docker applications
* [MySQL](https://www.mysql.com/) - Relational database management system
* [JUnit](https://junit.org/junit5/) - Unit tests tool in Java
* [Mockito](https://site.mockito.org/) - Mocking framework for unit tests in Java

## Running Project
### Dockerized application with dev profile
To run and build docker-compose containers with JDK and MySQL:
```sh
docker-compose up --build
```

Dockerized application will be launched with active `dev` profile. 

Properties file `application-dev.properties` will be loaded.

## Fixtures
### Fixtures in Dev profile
This profile contains `fixture.enabled` property set to true. 

This enables initial population of dockerized MySQL database with pre-defined set of `Languages` and parsed from `.csv` fixture files entities of `Attributes` and `Options` with translations and required relationships.

## Tests
Project cosists of integration and unit tests. All are ran using `tests` profile.

Properties file  `application-tests.properties` will be loaded.

Integration tests are executed with the use of in-memory H2 database.

## Features
* REST management of `Attribute` and `AttributeTranslations`
* REST management of `Option`
* REST management of relationship between `Attribute` and `Option`

## Detailed Attribute JSON view
Available from endpoint `GET` `/attributes/{id}/detailed`
```json
{
    "id": 1,
    "code": "color",
    "translations": [
        {
            "locale": "pl_PL",
            "label": "Kolor"
        }
    ],
    "options": [
        {
            "id": 1,
            "code": "green",
            "sortOrder": 0,
            "translations": [
                {
                    "locale": "pl_PL",
                    "label": "Zielony"
                }
            ]
        },
        {
            "id": 2,
            "code": "blue",
            "sortOrder": 1,
            "translations": [
                {
                    "locale": "pl_PL",
                    "label": "Niebieski"
                }
            ]
        }
    ]
}
```

## To Do:
* Endpoints for `Language` management