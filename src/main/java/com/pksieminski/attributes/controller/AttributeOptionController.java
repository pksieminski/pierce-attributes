package com.pksieminski.attributes.controller;

import javax.validation.Valid;

import com.pksieminski.attributes.dto.model.AttributeOptionDTO;
import com.pksieminski.attributes.dto.model.RelateAttributeOptionDTO;
import com.pksieminski.attributes.service.AttributeOptionService;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AttributeOptionController {

    private final AttributeOptionService attributeOptionService;

    AttributeOptionController(AttributeOptionService attributeOptionService) {
        this.attributeOptionService = attributeOptionService;
    }

    @PutMapping("/attributes/{id}/options/{optionId}")
    AttributeOptionDTO relateAttributeWithOption(
        @Valid @RequestBody RelateAttributeOptionDTO dto, 
        @PathVariable Long id,
        @PathVariable Long optionId
    ) {
        return attributeOptionService.relateAttributeWithOption(dto, id, optionId);
    }

    @DeleteMapping("/attributes/{id}/options/{optionId}")
    void unrelateAttributeWithOption(
        @PathVariable Long id,
        @PathVariable Long optionId
    ) {
        attributeOptionService.unrelateAttributeWithOption(id, optionId);
    }
}