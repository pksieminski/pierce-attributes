package com.pksieminski.attributes.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.pksieminski.attributes.dto.model.AttributeDTO;
import com.pksieminski.attributes.dto.model.AttributeTranslationDTO;
import com.pksieminski.attributes.dto.model.CreateAttributeDTO;
import com.pksieminski.attributes.dto.model.DetailedAttributeDTO;
import com.pksieminski.attributes.dto.model.UpdateAttributeTranslationDTO;

import java.util.List;

import javax.validation.Valid;

import com.pksieminski.attributes.service.AttributeService;

@RestController
public class AttributeController {

    private final AttributeService attributeService;

    AttributeController(AttributeService attributeService) {
        this.attributeService = attributeService;
    }

    @GetMapping("/attributes")
    List<AttributeDTO> getAttributes() {
        return attributeService.findAll();
    }

    @PostMapping("/attributes")
    AttributeDTO createAttribute(@Valid @RequestBody CreateAttributeDTO dto) {
        return attributeService.create(dto);
    }

    @GetMapping("/attributes/{id}")
    AttributeDTO getAttribute(@PathVariable Long id) {
        return attributeService.findById(id);
    }

    @GetMapping("/attributes/{id}/detailed")
    DetailedAttributeDTO getAttributeDetailed(@PathVariable Long id) {
        return attributeService.findDetailedById(id);
    }

    @DeleteMapping("/attributes/{id}")
    void deleteAttribute(@PathVariable Long id) {
        attributeService.deleteById(id);
    }

    @GetMapping("/attributes/{id}/translations")
    List<AttributeTranslationDTO> getAttributeTranslations(@PathVariable Long id) {
        return attributeService.findAllAttributeTranslations(id);
    }

    @PostMapping("/attributes/{id}/translations")
    AttributeTranslationDTO createAttributeTranslation(
        @Valid @RequestBody AttributeTranslationDTO dto, 
        @PathVariable Long id
    ) {
        return attributeService.createTranslation(dto, id);
    }

    @PatchMapping("/attributes/{id}/translations/{locale:[a-z]{2}_[A-Z]{2}}")
    AttributeTranslationDTO updateAttributeTranslation(
        @PathVariable Long id, 
        @PathVariable String locale,
        @RequestBody UpdateAttributeTranslationDTO dto
    ) {
        return attributeService.updateTranslation(dto, id, locale);
    }

    @DeleteMapping("/attributes/{id}/translations/{locale:[a-z]{2}_[A-Z]{2}}")
    void deleteAttributeTranslation(@PathVariable Long id, @PathVariable String locale) {
        attributeService.deleteTranslation(id, locale);
    }
}