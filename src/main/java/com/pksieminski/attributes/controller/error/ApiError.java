package com.pksieminski.attributes.controller.error;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class ApiError {

    @NonNull private Integer status;
    @NonNull private String error;
    @NonNull private String message;
    private List<String> errors = new ArrayList<>();
}