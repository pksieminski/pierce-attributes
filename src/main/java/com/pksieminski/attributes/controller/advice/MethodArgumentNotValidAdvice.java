package com.pksieminski.attributes.controller.advice;

import java.util.ArrayList;
import java.util.List;

import com.pksieminski.attributes.controller.error.ApiError;

import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
class MethodArgumentNotValidAdvice {

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    ApiError employeeNotFoundHandler(MethodArgumentNotValidException exception) {
        List<String> errors = new ArrayList<>();

		for (ObjectError error : exception.getBindingResult().getAllErrors()) {
			errors.add(error.toString());
		}

        return new ApiError(
            HttpStatus.BAD_REQUEST.value(),
            HttpStatus.BAD_REQUEST.getReasonPhrase(),
            "Validation failed",
            errors
        );
    }
}