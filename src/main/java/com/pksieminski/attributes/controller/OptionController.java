package com.pksieminski.attributes.controller;

import java.util.List;

import javax.validation.Valid;

import com.pksieminski.attributes.dto.model.CreateOptionDTO;
import com.pksieminski.attributes.dto.model.OptionDTO;
import com.pksieminski.attributes.service.OptionService;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OptionController {

    private final OptionService optionService;

    public OptionController(OptionService optionService) {
        this.optionService = optionService;
    }

    @GetMapping("/options")
    List<OptionDTO> getOptions() {
        return optionService.findAll();
    }

    @PostMapping("/options")
    OptionDTO createOption(@Valid @RequestBody CreateOptionDTO dto) {
        return optionService.create(dto);
    }

    @GetMapping("/options/{id}")
    OptionDTO getOption(@PathVariable Long id) {
        return optionService.findById(id);
    }

    @DeleteMapping("/options/{id}")
    void deleteOption(@PathVariable Long id) {
        optionService.deleteById(id);
    }
}