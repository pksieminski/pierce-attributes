package com.pksieminski.attributes;

import com.pksieminski.attributes.property.FixtureProperties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({FixtureProperties.class})
public class AttributesApplication {

	public static void main(String[] args) {
		SpringApplication.run(AttributesApplication.class, args);
	}

}
