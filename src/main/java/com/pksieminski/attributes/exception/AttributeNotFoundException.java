package com.pksieminski.attributes.exception;

public class AttributeNotFoundException extends EntityNotFoundException {

    public AttributeNotFoundException(Long id) {
        super(String.format("Could not find attribute %d", id));
    }
}