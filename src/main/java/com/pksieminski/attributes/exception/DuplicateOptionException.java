package com.pksieminski.attributes.exception;

public class DuplicateOptionException extends DuplicateEntityException {

    public DuplicateOptionException(String code) {
        super(String.format("Duplicate option for code %s", code));
    }
}