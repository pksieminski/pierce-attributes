package com.pksieminski.attributes.exception;

public class DuplicateAttributeException extends DuplicateEntityException {

    public DuplicateAttributeException(String code) {
        super(String.format("Duplicate attribute for code %s", code));
    }
}