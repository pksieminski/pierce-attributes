package com.pksieminski.attributes.exception;

public class DuplicateAttributeTranslationException extends DuplicateEntityException {

    public DuplicateAttributeTranslationException(Long attributeId, String locale) {
        super(String.format("Duplicate translation for attribute %d with locale %s", attributeId, locale));
    }
}