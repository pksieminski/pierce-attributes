package com.pksieminski.attributes.exception;

public class AttributeTranslationNotFoundException extends EntityNotFoundException {

    public AttributeTranslationNotFoundException(Long id, String locale) {
        super(String.format("Could not find translation for attribute %d with locale %s", id, locale));
    }
}