package com.pksieminski.attributes.exception;

public class OptionNotFoundException extends EntityNotFoundException {

    public OptionNotFoundException(Long id) {
        super(String.format("Could not find option %d", id));
    }
}