package com.pksieminski.attributes.exception;

public class LanguageNotFoundException extends EntityNotFoundException {

    public LanguageNotFoundException(Long id) {
        super(String.format("Could not find language %d", id));
    }

    public LanguageNotFoundException(String locale) {
        super(String.format("Could not find language with locale %s", locale));
    }
}