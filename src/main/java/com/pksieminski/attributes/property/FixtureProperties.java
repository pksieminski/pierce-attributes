package com.pksieminski.attributes.property;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties(prefix = "fixture")
public class FixtureProperties {
    private String attributeDir;
    private String optionDir;
    private Boolean enabled = false;
}