package com.pksieminski.attributes.loader;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import org.springframework.core.annotation.Order;

import java.nio.file.Paths;
import java.util.List;

import com.pksieminski.attributes.csv.bean.CSVOption;
import com.pksieminski.attributes.csv.mapper.CSVOptionMapper;
import com.pksieminski.attributes.entity.Option;
import com.pksieminski.attributes.csv.parser.CSVToBeanParser;
import com.pksieminski.attributes.property.FixtureProperties;
import com.pksieminski.attributes.repository.OptionRepository;

@Slf4j
@Order(3)
@Component
public class OptionFixtureLoader implements CommandLineRunner {

    private final FixtureProperties fixtureProperties;
    private final OptionRepository optionRepository;
    private final CSVOptionMapper csvOptionMapper;

    public OptionFixtureLoader(
        FixtureProperties fixtureProperties,
        OptionRepository optionRepository,
        CSVOptionMapper csvOptionMapper
    ) {
        this.fixtureProperties = fixtureProperties;
        this.optionRepository = optionRepository;
        this.csvOptionMapper = csvOptionMapper;
    }

    @Override
    @Transactional
    public void run(String... strings) throws Exception {
        if (!fixtureProperties.getEnabled()) {
            return;
        }
        
        log.info("Loading option fixtures...");

        CSVToBeanParser<CSVOption> csvToBeanParser = new CSVToBeanParser<>(CSVOption.class);
        List<CSVOption> csvOptions = csvToBeanParser.parseDirectory(
            Paths.get(fixtureProperties.getOptionDir())
        );
        List<Option> options = csvOptionMapper.mapAll(csvOptions);
        optionRepository.saveAll(options);

        log.info("Finished loading option fixtures...");
    }
}