package com.pksieminski.attributes.loader;

import com.pksieminski.attributes.entity.Language;
import com.pksieminski.attributes.property.FixtureProperties;
import com.pksieminski.attributes.repository.LanguageRepository;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import org.springframework.core.annotation.Order;

@Slf4j
@Order(1)
@Component
public class LanguageFixtureLoader implements CommandLineRunner {

    private final FixtureProperties fixtureProperties;
    private final LanguageRepository languageRepository;

    private final String[] locales = {
        "es_ES",
        "nl_NL",
        "nl_BE",
        "nb_NO",
        "it_IT",
        "fr_FR",
        "fi_FI",
        "en_US",
        "sv_SE",
        "en_IE",
        "en_GB",
        "de_DE",
        "de_CH",
        "de_AT",
        "da_DK",
        "cs_CZ",
        "pl_PL"
    };

    public LanguageFixtureLoader(
        FixtureProperties fixtureProperties,
        LanguageRepository languageRepository
    ) {
        this.fixtureProperties = fixtureProperties;
        this.languageRepository = languageRepository;
    }

    @Override
    @Transactional
    public void run(String... strings) throws Exception {
        if (!fixtureProperties.getEnabled()) {
            return;
        }

        log.info("Loading language fixtures...");

        for (String locale : locales) {
            languageRepository.save(new Language(locale));
        }

        log.info("Finished loading language fixtures...");
    }
}