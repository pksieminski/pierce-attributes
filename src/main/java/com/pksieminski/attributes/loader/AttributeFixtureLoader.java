package com.pksieminski.attributes.loader;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import org.springframework.core.annotation.Order;

import java.nio.file.Paths;
import java.util.List;

import com.pksieminski.attributes.csv.bean.CSVAttribute;
import com.pksieminski.attributes.csv.mapper.CSVAttributeMapper;
import com.pksieminski.attributes.entity.Attribute;
import com.pksieminski.attributes.csv.parser.CSVToBeanParser;
import com.pksieminski.attributes.property.FixtureProperties;
import com.pksieminski.attributes.repository.AttributeRepository;

@Slf4j
@Order(2)
@Component
public class AttributeFixtureLoader implements CommandLineRunner {

    private final FixtureProperties fixtureProperties;
    private final AttributeRepository attributeRepository;
    private final CSVAttributeMapper csvAttributeMapper;

    public AttributeFixtureLoader(
        FixtureProperties fixtureProperties,
        AttributeRepository attributeRepository,
        CSVAttributeMapper csvAttributeMapper
    ) {
        this.fixtureProperties = fixtureProperties;
        this.attributeRepository = attributeRepository;
        this.csvAttributeMapper = csvAttributeMapper;
    }

    @Override
    @Transactional
    public void run(String... strings) throws Exception {
        if (!fixtureProperties.getEnabled()) {
            return;
        }
        
        log.info("Loading attribute fixtures...");

        CSVToBeanParser<CSVAttribute> csvToBeanParser = new CSVToBeanParser<>(CSVAttribute.class);
        List<CSVAttribute> csvAttributes= csvToBeanParser.parseDirectory(
            Paths.get(fixtureProperties.getAttributeDir())
        );
        List<Attribute> attributes = csvAttributeMapper.mapAll(csvAttributes);
        attributeRepository.saveAll(attributes);

        log.info("Finished loading attribute fixtures...");
    }
}