package com.pksieminski.attributes.service;

import java.util.Map;

import com.pksieminski.attributes.entity.Language;
import com.pksieminski.attributes.repository.LanguageRepository;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LanguageService {

    private final LanguageRepository languageRepository;

    public LanguageService(LanguageRepository languageRepository) {
        this.languageRepository = languageRepository;
    }

    @Transactional
    public Map<String, Language> findAllMapByLocale() {
        return languageRepository.findAllMapByLocale();
    }
}