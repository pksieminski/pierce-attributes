package com.pksieminski.attributes.service;

import java.util.Optional;

import javax.transaction.Transactional;

import com.pksieminski.attributes.dto.model.AttributeOptionDTO;
import com.pksieminski.attributes.dto.model.RelateAttributeOptionDTO;
import com.pksieminski.attributes.entity.Attribute;
import com.pksieminski.attributes.entity.AttributeOption;
import com.pksieminski.attributes.entity.AttributeOptionId;
import com.pksieminski.attributes.entity.Option;
import com.pksieminski.attributes.exception.AttributeNotFoundException;
import com.pksieminski.attributes.exception.OptionNotFoundException;
import com.pksieminski.attributes.repository.AttributeOptionRepository;
import com.pksieminski.attributes.repository.AttributeRepository;
import com.pksieminski.attributes.repository.OptionRepository;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class AttributeOptionService {

    private final AttributeRepository attributeRepository;
    private final OptionRepository optionRepository;
    private final AttributeOptionRepository attributeOptionRepository;
    private final ModelMapper modelMapper;

    public AttributeOptionService (
        AttributeRepository attributeRepository,
        OptionRepository optionRepository,
        AttributeOptionRepository attributeOptionRepository,
        ModelMapper modelMapper
    ) {
        this.attributeRepository = attributeRepository;
        this.optionRepository = optionRepository;
        this.attributeOptionRepository = attributeOptionRepository;
        this.modelMapper = modelMapper;
    }

    @Transactional
    public AttributeOptionDTO relateAttributeWithOption(RelateAttributeOptionDTO dto, Long attributeId, Long optionId) {
        Attribute attribute = attributeRepository
            .findById(attributeId)
            .orElseThrow(() -> new AttributeNotFoundException(attributeId));

        Option option = optionRepository
            .findById(optionId)
            .orElseThrow(() -> new OptionNotFoundException(optionId));

        Optional<AttributeOption> optionalAttributeOption = attributeOptionRepository
            .findById(new AttributeOptionId(attributeId, optionId));

        AttributeOption attributeOption;
        if (optionalAttributeOption.isPresent()) {
            attributeOption = optionalAttributeOption.get();
            attributeOption.setSortOrder(dto.getSortOrder());
        } else {
            attributeOption = new AttributeOption(attribute, option, dto.getSortOrder());
            option.getAttributeOptions().add(attributeOption);
        }

        return modelMapper.map(attributeOption, AttributeOptionDTO.class);
    }

    @Transactional
    public void unrelateAttributeWithOption(Long attributeId, Long optionId) {
        attributeOptionRepository.deleteById(new AttributeOptionId(attributeId, optionId));
    }
}