package com.pksieminski.attributes.service;

import java.util.List;
import java.util.stream.Collectors;

import com.pksieminski.attributes.dto.model.CreateOptionDTO;
import com.pksieminski.attributes.dto.model.OptionDTO;
import com.pksieminski.attributes.entity.Option;
import com.pksieminski.attributes.exception.DuplicateOptionException;
import com.pksieminski.attributes.exception.OptionNotFoundException;
import com.pksieminski.attributes.repository.OptionRepository;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OptionService {

    private final OptionRepository optionRepository;
    private final ModelMapper modelMapper;

    public OptionService(
        OptionRepository optionRepository,
        ModelMapper modelMapper
    ) {
        this.optionRepository = optionRepository;
        this.modelMapper = modelMapper;
    }

    @Transactional
    public List<OptionDTO> findAll() {
        return optionRepository
            .findAll()
            .stream()
            .map(a -> modelMapper.map(a, OptionDTO.class))
            .collect(Collectors.toList());
    }

    @Transactional
    public OptionDTO findById(Long id) {
        Option option = optionRepository
            .findById(id)
            .orElseThrow(() -> new OptionNotFoundException(id));

        return modelMapper.map(option, OptionDTO.class);
    }

    @Transactional
    public OptionDTO create(CreateOptionDTO dto) {
        if (optionRepository.existsByCode(dto.getCode())) {
            throw new DuplicateOptionException(dto.getCode());
        }

        Option option = modelMapper.map(dto, Option.class);
        optionRepository.save(option);

        return modelMapper.map(option, OptionDTO.class);
    }

    @Transactional
    public void deleteById(Long id) {
        optionRepository.deleteById(id);
    }
}