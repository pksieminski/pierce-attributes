package com.pksieminski.attributes.service;

import java.util.List;
import java.util.stream.Collectors;

import com.pksieminski.attributes.dto.model.AttributeDTO;
import com.pksieminski.attributes.dto.model.AttributeTranslationDTO;
import com.pksieminski.attributes.dto.model.CreateAttributeDTO;
import com.pksieminski.attributes.dto.model.DetailedAttributeDTO;
import com.pksieminski.attributes.dto.model.UpdateAttributeTranslationDTO;
import com.pksieminski.attributes.entity.Attribute;
import com.pksieminski.attributes.entity.AttributeTranslation;
import com.pksieminski.attributes.entity.Language;
import com.pksieminski.attributes.exception.AttributeNotFoundException;
import com.pksieminski.attributes.exception.AttributeTranslationNotFoundException;
import com.pksieminski.attributes.exception.DuplicateAttributeException;
import com.pksieminski.attributes.exception.DuplicateAttributeTranslationException;
import com.pksieminski.attributes.exception.LanguageNotFoundException;
import com.pksieminski.attributes.repository.AttributeRepository;
import com.pksieminski.attributes.repository.AttributeTranslationRepository;
import com.pksieminski.attributes.repository.LanguageRepository;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AttributeService {

    private final AttributeRepository attributeRepository;
    private final AttributeTranslationRepository attributeTranslationRepository;
    private final LanguageRepository languageRepository;
    private final ModelMapper modelMapper;

    public AttributeService(
        AttributeRepository attributeRepository,
        AttributeTranslationRepository attributeTranslationRepository,
        LanguageRepository languageRepository,
        ModelMapper modelMapper
    ) {
        this.attributeRepository = attributeRepository;
        this.attributeTranslationRepository = attributeTranslationRepository;
        this.languageRepository = languageRepository;
        this.modelMapper = modelMapper;
    }

    @Transactional
    public List<AttributeDTO> findAll() {
        return attributeRepository
            .findAll()
            .stream()
            .map(a -> modelMapper.map(a, AttributeDTO.class))
            .collect(Collectors.toList());
    }

    @Transactional
    public AttributeDTO findById(Long id) {
        return modelMapper.map(this.getAttribute(id), AttributeDTO.class);
    }

    @Transactional
    public DetailedAttributeDTO findDetailedById(Long id) {
        return modelMapper.map(getAttribute(id), DetailedAttributeDTO.class);
    }

    @Transactional
    public AttributeDTO create(CreateAttributeDTO dto) {
        if (attributeRepository.existsByCode(dto.getCode())) {
            throw new DuplicateAttributeException(dto.getCode());
        }

        Attribute attribute = modelMapper.map(dto, Attribute.class);
        attributeRepository.save(attribute);

        return modelMapper.map(attribute, AttributeDTO.class);
    }

    @Transactional
    public void deleteById(Long id) {
        attributeRepository.deleteById(id);
    }

    @Transactional
    public List<AttributeTranslationDTO> findAllAttributeTranslations(Long attributeId) {
        Attribute attribute = getAttribute(attributeId);

        return attribute
            .getAttributeTranslations()
            .stream()
            .map(a -> modelMapper.map(a, AttributeTranslationDTO.class))
            .collect(Collectors.toList());
    }

    @Transactional
    public AttributeTranslationDTO createTranslation(AttributeTranslationDTO dto, Long attributeId) {
        Attribute attribute = getAttribute(attributeId);
        Language language = languageRepository
            .findByLocale(dto.getLocale())
            .orElseThrow(() -> new LanguageNotFoundException(dto.getLocale()));

        if (attribute.hasTranslationInLanguage(language)) {
            throw new DuplicateAttributeTranslationException(attributeId, dto.getLocale());
        }

        AttributeTranslation translation = new AttributeTranslation(
            attribute,
            language,
            dto.getLabel()
        );
        attribute.getAttributeTranslations().add(translation);

        return modelMapper.map(translation, AttributeTranslationDTO.class);
    }

    @Transactional
    public AttributeTranslationDTO updateTranslation(
        UpdateAttributeTranslationDTO dto,
        Long attributeId,
        String locale
    ) {
        Attribute attribute = getAttribute(attributeId);
        AttributeTranslation translation = attribute
            .getTranslationForLocale(locale)
            .orElseThrow(() -> new AttributeTranslationNotFoundException(attributeId, locale));
        translation.setLabel(dto.getLabel());

        return modelMapper.map(translation, AttributeTranslationDTO.class);
    }

    @Transactional
    public void deleteTranslation(Long attributeId, String locale) {
        attributeTranslationRepository.deleteByAttributeIdAndLanguageLocale(attributeId, locale);
    }

    @Transactional
    private Attribute getAttribute(Long id) {
        return attributeRepository
            .findById(id)
            .orElseThrow(() -> new AttributeNotFoundException(id));
    }
}