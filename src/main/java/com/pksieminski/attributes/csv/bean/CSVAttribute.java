package com.pksieminski.attributes.csv.bean;

import com.opencsv.bean.CsvBindAndJoinByName;
import com.opencsv.bean.CsvBindByName;

import org.apache.commons.collections4.MultiValuedMap;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CSVAttribute {

    @CsvBindByName(column = "code", required = true)
    private String code;

    @CsvBindAndJoinByName(column = "label-[a-z]{2}_[A-Z]{2}", elementType = String.class)
    private MultiValuedMap<String, String> labels;
  }