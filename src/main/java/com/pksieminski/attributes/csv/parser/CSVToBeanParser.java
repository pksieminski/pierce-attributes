package com.pksieminski.attributes.csv.parser;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.NotDirectoryException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

public class CSVToBeanParser<T> {

    private final Class<T> genericType;

    public CSVToBeanParser(Class<T> genericType) {
        this.genericType = genericType;
    }

    public List<T> parseDirectory(Path dir) throws IOException {
        if (!Files.isDirectory(dir)) {
            throw new NotDirectoryException(String.format("Path %1$s is not a directory", dir));
        }

        Iterator<Path> iterator = Files.walk(dir)
            .filter(path -> path.toString().endsWith(".csv"))
            .iterator();

        List<T> csvBeans = new ArrayList<>();

        while (iterator.hasNext()) {
            Path path = iterator.next();
            csvBeans.addAll(this.parseFile(path));
        }

        return csvBeans;
    }

    public List<T> parseFile(Path file) throws IOException {
        if (!Files.isRegularFile(file)) {
            throw new FileNotFoundException(String.format("Path %1$s is not a regular file", file));
        }

        List<T> csvBeans = new ArrayList<>();

        try (
            Reader reader = Files.newBufferedReader(file);
        ) {
            CsvToBean<T> csvToBean = new CsvToBeanBuilder<T>(reader)
                .withType(genericType)
                .withIgnoreLeadingWhiteSpace(true)
                .withSeparator(';')
                .build();

            csvToBean.forEach(bean -> csvBeans.add(bean));
        } catch (IOException exception) {
            throw new IOException(String.format("Can't read CSV file %1$s", file));
        }

        return csvBeans;
    }
}