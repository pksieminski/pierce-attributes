package com.pksieminski.attributes.csv.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Map.Entry;

import com.pksieminski.attributes.csv.bean.CSVAttribute;
import com.pksieminski.attributes.entity.Attribute;
import com.pksieminski.attributes.entity.AttributeTranslation;
import com.pksieminski.attributes.entity.Language;
import com.pksieminski.attributes.repository.AttributeRepository;
import com.pksieminski.attributes.repository.LanguageRepository;

import org.springframework.stereotype.Service;

@Service
public class CSVAttributeMapper {

    private final AttributeRepository attributeRepository;
    private final LanguageRepository languageRepository;

    public CSVAttributeMapper(
        AttributeRepository attributeRepository,
        LanguageRepository languageRepository
    ) {
        this.attributeRepository = attributeRepository;
        this.languageRepository = languageRepository;
    }

    public List<Attribute> mapAll(List<CSVAttribute> csvAttributes) {
        List<Attribute> attributes = new ArrayList<>();
    
        for (CSVAttribute csvAttribute : csvAttributes) {
            Optional<Attribute> existingAttribute = attributes
                .stream()
                .filter(o -> o.getCode().equalsIgnoreCase(csvAttribute.getCode()))
                .findAny()
                .map(Optional::of)
                .orElse(attributeRepository.findByCode(csvAttribute.getCode()));

            if (!existingAttribute.isPresent()) {
                attributes.add(createFromCSV(csvAttribute));
            }
        }

        return attributes;
    }

    public Attribute createFromCSV(CSVAttribute csvAttribute) {
        Map<String, Language> languages = languageRepository.findAllMapByLocale();
        Attribute attribute = new Attribute(csvAttribute.getCode());
                
        for (Entry<String, String> entry : csvAttribute.getLabels().entries()) {
            if (entry.getValue().isEmpty()) {
                continue;
            }
            String locale = entry.getKey().replaceFirst("label-", "");

            if (!languages.containsKey(locale)) {
                continue;
            }
            Language language = languages.get(locale);

            Optional<AttributeTranslation> attributeTranslation = attribute
                .getAttributeTranslations()
                .stream()
                .filter(oT -> oT.getAttribute() == attribute && oT.getLanguage() == language)
                .findFirst();

            if (!attributeTranslation.isPresent()) {
                attribute.getAttributeTranslations().add(
                    new AttributeTranslation(
                        attribute, 
                        language, 
                        entry.getValue()
                    )
                );
            }
        }

        return attribute;
    }
}