package com.pksieminski.attributes.csv.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Map.Entry;

import com.pksieminski.attributes.csv.bean.CSVOption;
import com.pksieminski.attributes.entity.Attribute;
import com.pksieminski.attributes.entity.AttributeOption;
import com.pksieminski.attributes.entity.Language;
import com.pksieminski.attributes.entity.Option;
import com.pksieminski.attributes.entity.OptionTranslation;
import com.pksieminski.attributes.repository.AttributeRepository;
import com.pksieminski.attributes.repository.LanguageRepository;
import com.pksieminski.attributes.repository.OptionRepository;

import org.springframework.stereotype.Service;

@Service
public class CSVOptionMapper {

    private final OptionRepository optionRepository;
    private final AttributeRepository attributeRepository;
    private final LanguageRepository languageRepository;

    public CSVOptionMapper(
        OptionRepository optionRepository,
        AttributeRepository attributeRepository,
        LanguageRepository languageRepository
    ) {
        this.optionRepository = optionRepository;
        this.attributeRepository = attributeRepository;
        this.languageRepository = languageRepository;
    }

    public List<Option> mapAll(List<CSVOption> csvOptions) {
        List<Option> options = new ArrayList<>();
    
        for (CSVOption csvOption : csvOptions) {
            Optional<Option> existingOption = options
                .stream()
                .filter(o -> o.getCode().equalsIgnoreCase(csvOption.getCode()))
                .findAny()
                .map(Optional::of)
                .orElse(optionRepository.findByCode(csvOption.getCode()));

            Option option = existingOption.orElse(createFromCSV(csvOption));

            if (!existingOption.isPresent()) {
                options.add(option);
            }

            Optional<Attribute> attribute = attributeRepository.findByCode(csvOption.getAttribute());
            if (attribute.isPresent() && !attribute.get().hasOption(option)) {
                option.getAttributeOptions().add(
                    new AttributeOption(
                        attribute.get(),
                        option, 
                        csvOption.getSordOrder()
                    )
                );
            }
        }

        return options;
    }

    public Option createFromCSV(CSVOption csvOption) {
        Map<String, Language> languages = languageRepository.findAllMapByLocale();
        Option option = new Option(csvOption.getCode());

        for (Entry<String, String> entry : csvOption.getLabels().entries()) {
            if (entry.getValue().isEmpty()) {
                continue;
            }
            String locale = entry.getKey().replaceFirst("label-", "");

            if (!languages.containsKey(locale)) {
                continue;
            }
            Language language = languages.get(locale);

            Optional<OptionTranslation> optionTranslation = option
                .getOptionTranslations()
                .stream()
                .filter(oT -> oT.getOption() == option && oT.getLanguage() == language)
                .findFirst();

            if (!optionTranslation.isPresent()) {
                option.getOptionTranslations().add(
                    new OptionTranslation(
                        option, 
                        language, 
                        entry.getValue()
                    )
                );
            }
        }

        return option;
    }
}