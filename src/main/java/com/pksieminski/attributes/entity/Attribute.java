package com.pksieminski.attributes.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.NonNull;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.persistence.CascadeType;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
@Entity
@NoArgsConstructor
@RequiredArgsConstructor
public class Attribute {

    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @NotBlank(message = "Code is mandatory")
    @Column(nullable = false, unique = true)
    private String code;

    @OneToMany(mappedBy = "attribute", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<AttributeTranslation> attributeTranslations = new ArrayList<>();

    @OneToMany(mappedBy = "attribute", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<AttributeOption> attributeOptions = new ArrayList<>();

    public boolean hasOption(Option option) {
        return option
            .getAttributeOptions()
            .stream()
            .filter(ao -> ao.getAttribute() == this)
            .findAny()
            .isPresent();
    }

    public boolean hasTranslationInLanguage(Language language) {
        return this
            .getAttributeTranslations()
            .stream()
            .filter(at -> at.getLanguage() == language)
            .findAny()
            .isPresent();
    }

    public Optional<AttributeTranslation> getTranslationForLocale(String locale) {
        return this
            .getAttributeTranslations()
            .stream()
            .filter(at -> at.getLanguage().getLocale().equals(locale))
            .findAny();
    }
}