package com.pksieminski.attributes.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import java.io.Serializable;

@Data
@Embeddable
@NoArgsConstructor
@RequiredArgsConstructor
public class AttributeOptionId implements Serializable {
 
    @NonNull
    @Column(name = "attribute_id")
    private Long attributeId;

    @NonNull
    @Column(name = "option_id")
    private Long optionId;
}