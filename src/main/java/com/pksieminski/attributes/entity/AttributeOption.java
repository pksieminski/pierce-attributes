package com.pksieminski.attributes.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.NonNull;

import javax.persistence.Entity;
import javax.persistence.EmbeddedId;
import javax.persistence.Column;
import javax.persistence.MapsId;
import javax.persistence.ManyToOne;

@Data
@Entity
@NoArgsConstructor
@RequiredArgsConstructor
public class AttributeOption {

    @EmbeddedId
    private AttributeOptionId id = new AttributeOptionId();

    @NonNull
    @ManyToOne(optional = false)
    @MapsId("attributeId")
    private Attribute attribute;

    @NonNull
    @ManyToOne(optional = false)
    @MapsId("optionId")
    private Option option;

    @NonNull
    @Column(nullable = false)
    private Integer sortOrder;
}