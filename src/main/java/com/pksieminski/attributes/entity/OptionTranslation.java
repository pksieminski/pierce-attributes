package com.pksieminski.attributes.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EmbeddedId;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

@Data
@Entity
@NoArgsConstructor
@RequiredArgsConstructor
public class OptionTranslation {

    @EmbeddedId
    private OptionTranslationId id = new OptionTranslationId();

    @NonNull
    @ManyToOne(optional = false)
    @MapsId("optionId")
    private Option option;
 
    @NonNull
    @ManyToOne(optional = false)
    @MapsId("languageId")
    private Language language;

    @NonNull
    @Column(nullable = false)
    private String label;
}