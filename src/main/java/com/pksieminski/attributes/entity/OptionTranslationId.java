package com.pksieminski.attributes.entity;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;

import java.io.Serializable;

@Data
@Embeddable
public class OptionTranslationId implements Serializable {

    @JoinColumn(name = "option_id")
    private Long optionId;

    @JoinColumn(name = "language_id")
    private Long languageId;
}