package com.pksieminski.attributes.entity;

import lombok.Data;

import javax.persistence.JoinColumn;
import javax.persistence.Embeddable;

import java.io.Serializable;

@Data
@Embeddable
public class AttributeTranslationId implements Serializable {
 
    @JoinColumn(name = "attribute_id")
    private Long attributeId;

    @JoinColumn(name = "language_id")
    private Long languageId;
}