package com.pksieminski.attributes.repository;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.pksieminski.attributes.entity.Language;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LanguageRepository extends JpaRepository<Language, Long> {

    public Optional<Language> findByLocale(String locale);
    
    default Map<String, Language> findAllMapByLocale() {
        return findAll().stream().collect(Collectors.toMap(Language::getLocale, v -> v));
    }
}