package com.pksieminski.attributes.repository;

import com.pksieminski.attributes.entity.AttributeTranslation;
import com.pksieminski.attributes.entity.AttributeTranslationId;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AttributeTranslationRepository 
    extends JpaRepository<AttributeTranslation, AttributeTranslationId> {
        
        public void deleteByAttributeIdAndLanguageLocale(Long attributeId, String locale);
}