package com.pksieminski.attributes.repository;

import java.util.Optional;

import com.pksieminski.attributes.entity.Attribute;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AttributeRepository extends JpaRepository<Attribute, Long> {

    boolean existsByCode(String code);
    Optional<Attribute> findByCode(String code);
}