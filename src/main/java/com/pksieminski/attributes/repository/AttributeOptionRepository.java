package com.pksieminski.attributes.repository;

import com.pksieminski.attributes.entity.AttributeOption;
import com.pksieminski.attributes.entity.AttributeOptionId;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AttributeOptionRepository extends JpaRepository<AttributeOption, AttributeOptionId> {

}