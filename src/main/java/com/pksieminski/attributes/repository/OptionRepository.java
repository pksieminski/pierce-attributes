package com.pksieminski.attributes.repository;

import java.util.Optional;

import com.pksieminski.attributes.entity.Option;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OptionRepository extends JpaRepository<Option, Long> {
    
    boolean existsByCode(String code);
    Optional<Option> findByCode(String code);
}