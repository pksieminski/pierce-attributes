package com.pksieminski.attributes.configuration;

import org.modelmapper.AbstractConverter;
import org.modelmapper.ModelMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfiguration {
    
    @Bean
    @SuppressWarnings("unchecked")
    public ModelMapper modelMapper(ApplicationContext applicationContext) {
        ModelMapper modelMapper = new ModelMapper();
        applicationContext
            .getBeansOfType(AbstractConverter.class)
            .forEach((k, v) -> modelMapper.addConverter(v));

        return modelMapper;
    }
}