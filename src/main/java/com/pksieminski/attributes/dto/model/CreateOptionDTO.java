package com.pksieminski.attributes.dto.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class CreateOptionDTO {

    @NotBlank
    private String code;

    private List<OptionTranslationDTO> translations = new ArrayList<>();
}