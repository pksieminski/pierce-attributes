package com.pksieminski.attributes.dto.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class AttributeDTO {

    private Long id;

    private String code;

    private List<AttributeTranslationDTO> translations = new ArrayList<>();
}