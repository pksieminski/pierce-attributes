package com.pksieminski.attributes.dto.model;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class UpdateAttributeTranslationDTO {

    @NotBlank
    private String label;
}