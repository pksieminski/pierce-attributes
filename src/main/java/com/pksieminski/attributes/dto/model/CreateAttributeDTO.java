package com.pksieminski.attributes.dto.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class CreateAttributeDTO {

    @NotBlank
    private String code;

    private List<AttributeTranslationDTO> translations = new ArrayList<>();
}