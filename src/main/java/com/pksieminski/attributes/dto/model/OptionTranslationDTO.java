package com.pksieminski.attributes.dto.model;

import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
public class OptionTranslationDTO {

    private String label;

    @Pattern(message = "Invalid locale format", regexp = "[a-z]{2}_[A-Z]{2}")
    private String locale;
}