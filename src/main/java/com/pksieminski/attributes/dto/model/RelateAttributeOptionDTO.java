package com.pksieminski.attributes.dto.model;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class RelateAttributeOptionDTO {

    @NotNull
    private Integer sortOrder;
}