package com.pksieminski.attributes.dto.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class DetailedAttributeDTO {

    private Long id;

    private String code;

    private List<AttributeTranslationDTO> translations = new ArrayList<>();

    private List<AttributeOptionDTO> options = new ArrayList<>();
}