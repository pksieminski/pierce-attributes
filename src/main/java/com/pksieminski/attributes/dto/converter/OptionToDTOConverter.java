package com.pksieminski.attributes.dto.converter;

import com.pksieminski.attributes.dto.model.OptionDTO;
import com.pksieminski.attributes.dto.model.OptionTranslationDTO;
import com.pksieminski.attributes.entity.Option;

import org.modelmapper.AbstractConverter;
import org.springframework.stereotype.Component;

@Component
public class OptionToDTOConverter extends AbstractConverter<Option, OptionDTO> {
    
    @Override
    protected OptionDTO convert(Option option) {
        OptionDTO dto = new OptionDTO();
        dto.setId(option.getId());
        dto.setCode(option.getCode());

        option
            .getOptionTranslations()
            .forEach(t -> {
                OptionTranslationDTO translationDTO = new OptionTranslationDTO();
                translationDTO.setLabel(t.getLabel());
                translationDTO.setLocale(t.getLanguage().getLocale());
                dto.getTranslations().add(translationDTO);
            });

        return dto;
    }
}