package com.pksieminski.attributes.dto.converter;

import java.util.List;
import java.util.stream.Collectors;

import com.pksieminski.attributes.dto.model.AttributeOptionDTO;
import com.pksieminski.attributes.dto.model.AttributeTranslationDTO;
import com.pksieminski.attributes.dto.model.DetailedAttributeDTO;
import com.pksieminski.attributes.dto.model.OptionTranslationDTO;
import com.pksieminski.attributes.entity.Attribute;
import com.pksieminski.attributes.entity.AttributeOption;

import org.modelmapper.AbstractConverter;
import org.springframework.stereotype.Component;

@Component
public class AttributeToDetailedDTOConverter extends AbstractConverter<Attribute, DetailedAttributeDTO> {

    @Override
    protected DetailedAttributeDTO convert(Attribute attribute) {
        DetailedAttributeDTO dto = new DetailedAttributeDTO();
        dto.setId(attribute.getId());
        dto.setCode(attribute.getCode());

        attribute
            .getAttributeTranslations()
            .forEach(t -> {
                AttributeTranslationDTO translationDTO = new AttributeTranslationDTO();
                translationDTO.setLabel(t.getLabel());
                translationDTO.setLocale(t.getLanguage().getLocale());
                dto.getTranslations().add(translationDTO);
            });
        
        List<AttributeOptionDTO> options = attribute
            .getAttributeOptions()
            .stream()
            .sorted((AttributeOption ao1, AttributeOption ao2) -> ao1.getSortOrder().compareTo(ao2.getSortOrder()))
            .map(ao -> convert(ao))
            .collect(Collectors.toList());

        dto.setOptions(options);

        return dto;
    }

    private AttributeOptionDTO convert(AttributeOption attributeOption) {
        AttributeOptionDTO dto = new AttributeOptionDTO();
        dto.setId(attributeOption.getOption().getId());
        dto.setCode(attributeOption.getOption().getCode());
        dto.setSortOrder(attributeOption.getSortOrder());

        attributeOption.getOption()
            .getOptionTranslations()
            .forEach(t -> {
                OptionTranslationDTO translationDTO = new OptionTranslationDTO();
                translationDTO.setLabel(t.getLabel());
                translationDTO.setLocale(t.getLanguage().getLocale());
                dto.getTranslations().add(translationDTO);
            });

        return dto;
    }
}