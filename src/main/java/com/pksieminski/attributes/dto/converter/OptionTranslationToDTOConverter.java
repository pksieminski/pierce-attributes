package com.pksieminski.attributes.dto.converter;

import com.pksieminski.attributes.dto.model.OptionTranslationDTO;
import com.pksieminski.attributes.entity.OptionTranslation;

import org.modelmapper.AbstractConverter;
import org.springframework.stereotype.Component;

@Component
public class OptionTranslationToDTOConverter 
    extends AbstractConverter<OptionTranslation, OptionTranslationDTO> {
    
    @Override
    protected OptionTranslationDTO convert(OptionTranslation optionTranslation) {
        OptionTranslationDTO dto = new OptionTranslationDTO();
        dto.setLabel(optionTranslation.getLabel());
        dto.setLocale(optionTranslation.getLanguage().getLocale());

        return dto;
    }
}