package com.pksieminski.attributes.dto.converter;

import java.util.Map;

import com.pksieminski.attributes.dto.model.OptionTranslationDTO;
import com.pksieminski.attributes.dto.model.CreateOptionDTO;
import com.pksieminski.attributes.entity.Option;
import com.pksieminski.attributes.entity.OptionTranslation;
import com.pksieminski.attributes.entity.Language;
import com.pksieminski.attributes.repository.LanguageRepository;

import org.modelmapper.AbstractConverter;
import org.springframework.stereotype.Component;

@Component
public class CreateOptionDTOToOptionConverter extends AbstractConverter<CreateOptionDTO, Option> {

    private final LanguageRepository languageRepository;

    public CreateOptionDTOToOptionConverter(LanguageRepository languageRepository) {
        this.languageRepository = languageRepository;
    }
    
    @Override
    protected Option convert(CreateOptionDTO dto) {
        Option option = new Option(dto.getCode());
        Map<String, Language> languages = languageRepository.findAllMapByLocale();

        for (OptionTranslationDTO translationDTO : dto.getTranslations()) {
            if (!languages.containsKey(translationDTO.getLocale())) {
                continue;
            }
            Language language = languages.get(translationDTO.getLocale());

            option.getOptionTranslations().add(
                new OptionTranslation(
                    option,
                    language,
                    translationDTO.getLabel()
                )
            );
        }

        return option;
    }
}