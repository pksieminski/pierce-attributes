package com.pksieminski.attributes.dto.converter;

import java.util.Map;

import com.pksieminski.attributes.dto.model.AttributeTranslationDTO;
import com.pksieminski.attributes.dto.model.CreateAttributeDTO;
import com.pksieminski.attributes.entity.Attribute;
import com.pksieminski.attributes.entity.AttributeTranslation;
import com.pksieminski.attributes.entity.Language;
import com.pksieminski.attributes.repository.LanguageRepository;

import org.modelmapper.AbstractConverter;
import org.springframework.stereotype.Component;

@Component
public class CreateAttributeDTOToAttributeConverter extends AbstractConverter<CreateAttributeDTO, Attribute> {

    private final LanguageRepository languageRepository;

    public CreateAttributeDTOToAttributeConverter(LanguageRepository languageRepository) {
        this.languageRepository = languageRepository;
    }
    
    @Override
    protected Attribute convert(CreateAttributeDTO dto) {
        Attribute attribute = new Attribute(dto.getCode());
        Map<String, Language> languages = languageRepository.findAllMapByLocale();

        for (AttributeTranslationDTO translationDTO : dto.getTranslations()) {
            if (!languages.containsKey(translationDTO.getLocale())) {
                continue;
            }
            Language language = languages.get(translationDTO.getLocale());

            attribute.getAttributeTranslations().add(
                new AttributeTranslation(
                    attribute,
                    language,
                    translationDTO.getLabel()
                )
            );
        }

        return attribute;
    }
}