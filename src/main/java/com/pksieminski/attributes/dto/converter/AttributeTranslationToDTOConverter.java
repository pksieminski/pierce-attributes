package com.pksieminski.attributes.dto.converter;

import com.pksieminski.attributes.dto.model.AttributeTranslationDTO;
import com.pksieminski.attributes.entity.AttributeTranslation;

import org.modelmapper.AbstractConverter;
import org.springframework.stereotype.Component;

@Component
public class AttributeTranslationToDTOConverter 
    extends AbstractConverter<AttributeTranslation, AttributeTranslationDTO> {
    
    @Override
    protected AttributeTranslationDTO convert(AttributeTranslation attributeTranslation) {
        AttributeTranslationDTO dto = new AttributeTranslationDTO();
        dto.setLabel(attributeTranslation.getLabel());
        dto.setLocale(attributeTranslation.getLanguage().getLocale());

        return dto;
    }
}