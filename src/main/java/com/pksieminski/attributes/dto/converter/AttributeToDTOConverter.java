package com.pksieminski.attributes.dto.converter;

import com.pksieminski.attributes.dto.model.AttributeDTO;
import com.pksieminski.attributes.dto.model.AttributeTranslationDTO;
import com.pksieminski.attributes.entity.Attribute;

import org.modelmapper.AbstractConverter;
import org.springframework.stereotype.Component;

@Component
public class AttributeToDTOConverter extends AbstractConverter<Attribute, AttributeDTO> {
    
    @Override
    protected AttributeDTO convert(Attribute attribute) {
        AttributeDTO dto = new AttributeDTO();
        dto.setId(attribute.getId());
        dto.setCode(attribute.getCode());

        attribute
            .getAttributeTranslations()
            .forEach(t -> {
                AttributeTranslationDTO translationDTO = new AttributeTranslationDTO();
                translationDTO.setLabel(t.getLabel());
                translationDTO.setLocale(t.getLanguage().getLocale());
                dto.getTranslations().add(translationDTO);
            });

        return dto;
    }
}