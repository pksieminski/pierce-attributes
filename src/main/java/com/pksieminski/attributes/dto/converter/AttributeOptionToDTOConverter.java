package com.pksieminski.attributes.dto.converter;

import com.pksieminski.attributes.dto.model.AttributeOptionDTO;
import com.pksieminski.attributes.dto.model.OptionTranslationDTO;
import com.pksieminski.attributes.entity.AttributeOption;

import org.modelmapper.AbstractConverter;
import org.springframework.stereotype.Component;

@Component
public class AttributeOptionToDTOConverter extends AbstractConverter<AttributeOption, AttributeOptionDTO> {
    
    @Override
    protected AttributeOptionDTO convert(AttributeOption attributeOption) {
        AttributeOptionDTO dto = new AttributeOptionDTO();
        dto.setId(attributeOption.getOption().getId());
        dto.setCode(attributeOption.getOption().getCode());
        dto.setSortOrder(attributeOption.getSortOrder());

        attributeOption.getOption()
            .getOptionTranslations()
            .forEach(t -> {
                OptionTranslationDTO translationDTO = new OptionTranslationDTO();
                translationDTO.setLabel(t.getLabel());
                translationDTO.setLocale(t.getLanguage().getLocale());
                dto.getTranslations().add(translationDTO);
            });

        return dto;
    }
}