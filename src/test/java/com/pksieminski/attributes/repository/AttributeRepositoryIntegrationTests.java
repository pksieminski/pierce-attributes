package com.pksieminski.attributes.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import com.pksieminski.attributes.entity.Attribute;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@DataJpaTest
@ActiveProfiles("tests")
@RunWith(SpringRunner.class)
public class AttributeRepositoryIntegrationTests {
 
    @Autowired
    private TestEntityManager entityManager;
 
    @Autowired
    private AttributeRepository attributeRepository;
 
    @Test
	public void whenExistsByCode_thenReturnTrue() {
		Attribute attribute = new Attribute("size");
		entityManager.persist(attribute);
		entityManager.flush();

		assertTrue(attributeRepository.existsByCode(attribute.getCode()));
	}

	@Test
	public void whenExistsByCode_thenReturnFalse() {
		assertFalse(attributeRepository.existsByCode("size"));
	}

	@Test
	public void whenFindByCode_thenReturnAttribute() {
		Attribute attribute = new Attribute("size");
		entityManager.persist(attribute);
		entityManager.flush();

		Optional<Attribute> optional = attributeRepository.findByCode(attribute.getCode());

		assertEquals(attribute, optional.get());
	}
}