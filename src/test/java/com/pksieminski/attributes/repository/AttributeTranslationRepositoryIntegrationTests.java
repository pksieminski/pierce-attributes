package com.pksieminski.attributes.repository;

import static org.junit.Assert.assertEquals;

import com.pksieminski.attributes.entity.Attribute;
import com.pksieminski.attributes.entity.AttributeTranslation;
import com.pksieminski.attributes.entity.Language;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@DataJpaTest
@ActiveProfiles("tests")
@RunWith(SpringRunner.class)
public class AttributeTranslationRepositoryIntegrationTests {
 
    @Autowired
    private TestEntityManager entityManager;
 
    @Autowired
    private AttributeTranslationRepository attributeTranslationRepository;
 
    @Test
	public void whenDeleteByAttributeIdAndLanguageLocale_thenDeleted() {
		Attribute attribute = new Attribute("size");
		Language language = new Language("pl_PL");
		AttributeTranslation attributeTranslation = new AttributeTranslation(attribute, language, "Rozmiar");

		entityManager.persist(attribute);
		entityManager.persist(language);
		entityManager.persist(attributeTranslation);
		entityManager.flush();

		attributeTranslationRepository.deleteByAttributeIdAndLanguageLocale(
			attribute.getId(), 
			language.getLocale()
		);

		assertEquals(0, attributeTranslationRepository.count());
	}
}