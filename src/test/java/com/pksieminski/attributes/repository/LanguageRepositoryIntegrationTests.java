package com.pksieminski.attributes.repository;

import static org.junit.Assert.assertEquals;

import java.util.Map;
import java.util.Optional;

import com.pksieminski.attributes.entity.Language;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@DataJpaTest
@ActiveProfiles("tests")
@RunWith(SpringRunner.class)
public class LanguageRepositoryIntegrationTests {
 
    @Autowired
    private TestEntityManager entityManager;
 
    @Autowired
    private LanguageRepository languageRepository;

	@Test
	public void whenFindByLocale_thenReturnLanguage() {
		Language language = new Language("pl_PL");
		entityManager.persist(language);
		entityManager.flush();

		Optional<Language> optional = languageRepository.findByLocale(language.getLocale());

		assertEquals(language, optional.get());
    }
    
    @Test
    public void whenFindAllMapByLocale_thenReturnAllMappedByLocale() {
        Language firstLanguage = new Language("pl_PL");
        Language secondLanguage = new Language("en_GB");
        entityManager.persist(firstLanguage);
        entityManager.persist(secondLanguage);
        entityManager.flush();
        
        Map<String, Language> map = languageRepository.findAllMapByLocale();

        assertEquals(2, map.size());
        assertEquals(map.get(firstLanguage.getLocale()), firstLanguage);
        assertEquals(map.get(secondLanguage.getLocale()), secondLanguage);
    }
}