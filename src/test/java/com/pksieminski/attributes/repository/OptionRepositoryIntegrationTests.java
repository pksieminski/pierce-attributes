package com.pksieminski.attributes.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import com.pksieminski.attributes.entity.Option;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@DataJpaTest
@ActiveProfiles("tests")
@RunWith(SpringRunner.class)
public class OptionRepositoryIntegrationTests {
 
    @Autowired
    private TestEntityManager entityManager;
 
    @Autowired
	private OptionRepository optionRepository;
	
	@Test
	public void whenExistsByCode_thenReturnTrue() {
		Option option = new Option("XL");
		entityManager.persist(option);
		entityManager.flush();

		assertTrue(optionRepository.existsByCode(option.getCode()));
	}

	@Test
	public void whenExistsByCode_thenReturnFalse() {
		assertFalse(optionRepository.existsByCode("XL"));
	}

	@Test
	public void whenFindByCode_thenReturnOption() {
		Option option = new Option("small");
		entityManager.persist(option);
		entityManager.flush();

		Optional<Option> optional = optionRepository.findByCode(option.getCode());

		assertEquals(option, optional.get());
	}
}