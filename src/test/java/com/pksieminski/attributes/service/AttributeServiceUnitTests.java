package com.pksieminski.attributes.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.pksieminski.attributes.dto.model.AttributeDTO;
import com.pksieminski.attributes.dto.model.AttributeTranslationDTO;
import com.pksieminski.attributes.dto.model.CreateAttributeDTO;
import com.pksieminski.attributes.dto.model.UpdateAttributeTranslationDTO;
import com.pksieminski.attributes.entity.Attribute;
import com.pksieminski.attributes.entity.AttributeTranslation;
import com.pksieminski.attributes.entity.Language;
import com.pksieminski.attributes.exception.AttributeNotFoundException;
import com.pksieminski.attributes.exception.AttributeTranslationNotFoundException;
import com.pksieminski.attributes.exception.DuplicateAttributeException;
import com.pksieminski.attributes.exception.DuplicateAttributeTranslationException;
import com.pksieminski.attributes.exception.LanguageNotFoundException;
import com.pksieminski.attributes.repository.AttributeRepository;
import com.pksieminski.attributes.repository.AttributeTranslationRepository;
import com.pksieminski.attributes.repository.LanguageRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("tests")
@RunWith(MockitoJUnitRunner.class)
public class AttributeServiceUnitTests {

    @InjectMocks
    private AttributeService attributeService;

    @Mock
    private AttributeRepository attributeRepository;

    @Mock
    private AttributeTranslationRepository attributeTranslationRepository;

    @Mock
    private LanguageRepository languageRepository;

    @Mock
    private ModelMapper modelMapper;
    
    @Test
    public void whenfindAll_thenReturnAllAsDTO() {
        Attribute firstAttribute = new Attribute("size");
        Attribute secondAttribute = new Attribute("color");
        List<Attribute> list = new ArrayList<>();
        list.add(firstAttribute);
        list.add(secondAttribute);

        AttributeDTO firstDTO = new AttributeDTO();
        AttributeDTO secondDTO = new AttributeDTO();

        doReturn(list).when(attributeRepository).findAll();
        doReturn(firstDTO).when(modelMapper).map(firstAttribute, AttributeDTO.class);
        doReturn(secondDTO).when(modelMapper).map(secondAttribute, AttributeDTO.class);
        
        List<AttributeDTO> dtoList = attributeService.findAll();

        verify(attributeRepository, times(1)).findAll();
        verify(modelMapper, times(1)).map(firstAttribute, AttributeDTO.class);
        verify(modelMapper, times(1)).map(secondAttribute, AttributeDTO.class);

        assertEquals(2, dtoList.size());
        assertTrue(dtoList.contains(firstDTO));
        assertTrue(dtoList.contains(secondDTO));
    }

    @Test
    public void whenFindById_thenReturnDTO() {
        Attribute attribute = new Attribute("size");
        attribute.setId(1L);
        Optional<Attribute> optional = Optional.of(attribute);
        AttributeDTO expectedDTO = new AttributeDTO();

        doReturn(optional).when(attributeRepository).findById(attribute.getId());
        doReturn(expectedDTO).when(modelMapper).map(attribute, AttributeDTO.class);

        AttributeDTO dto = attributeService.findById(attribute.getId());

        verify(attributeRepository, times(1)).findById(attribute.getId());
        verify(modelMapper, times(1)).map(attribute, AttributeDTO.class);

        assertEquals(expectedDTO, dto);
    }

    @Test(expected = AttributeNotFoundException.class)
    public void whenFindById_thenThrowException() {
        Long attributeId = 1L;

        doReturn(Optional.empty()).when(attributeRepository).findById(attributeId);

        attributeService.findById(attributeId);
    }

    @Test
    public void whenCreate_thenCreated() {
        CreateAttributeDTO createDTO = new CreateAttributeDTO();
        createDTO.setCode("size");
        Attribute attribute = new Attribute(createDTO.getCode());
        AttributeDTO expectedDTO = new AttributeDTO();

        doReturn(false).when(attributeRepository).existsByCode(createDTO.getCode());
        doReturn(attribute).when(modelMapper).map(createDTO, Attribute.class);
        doReturn(expectedDTO).when(modelMapper).map(attribute, AttributeDTO.class);

        AttributeDTO dto = attributeService.create(createDTO);

        verify(attributeRepository, times(1)).existsByCode(createDTO.getCode());
        verify(modelMapper, times(1)).map(createDTO, Attribute.class);
        verify(attributeRepository, times(1)).save(attribute);
        verify(modelMapper, times(1)).map(attribute, AttributeDTO.class);

        assertEquals(expectedDTO, dto);
    }

    @Test(expected = DuplicateAttributeException.class)
    public void whenCreate_thenThrowException() {
        CreateAttributeDTO createDTO = new CreateAttributeDTO();

        doReturn(true).when(attributeRepository).existsByCode(createDTO.getCode());

        attributeService.create(createDTO);
    }

    @Test()
    public void whenDeleteById_thenDelete() {
        Long id = 1L;

        attributeService.deleteById(id);

        verify(attributeRepository, times(1)).deleteById(id);
    }

    @Test
    public void whenFindAllAttributeTranslations_thenReturnAllAsDTO() {
        Long attributeId = 1L;
        Attribute attribute = mock(Attribute.class);
        Optional<Attribute> optional = Optional.of(attribute);
        AttributeTranslation firstTranslation = mock(AttributeTranslation.class);
        AttributeTranslation secondTranslation = mock(AttributeTranslation.class);
        List<AttributeTranslation> translations = new ArrayList<>();
        translations.add(firstTranslation);
        translations.add(secondTranslation);

        AttributeTranslationDTO firstDTO = new AttributeTranslationDTO();
        AttributeTranslationDTO secondDTO = new AttributeTranslationDTO();

        doReturn(optional).when(attributeRepository).findById(attributeId);
        doReturn(translations).when(attribute).getAttributeTranslations();
        doReturn(firstDTO).when(modelMapper).map(firstTranslation, AttributeTranslationDTO.class);
        doReturn(secondDTO).when(modelMapper).map(secondTranslation, AttributeTranslationDTO.class);
        
        List<AttributeTranslationDTO> dtoList = attributeService.findAllAttributeTranslations(attributeId);

        verify(attributeRepository, times(1)).findById(attributeId);
        verify(attribute, times(1)).getAttributeTranslations();
        verify(modelMapper, times(1)).map(firstTranslation, AttributeTranslationDTO.class);
        verify(modelMapper, times(1)).map(secondTranslation, AttributeTranslationDTO.class);

        assertEquals(2, dtoList.size());
        assertTrue(dtoList.contains(firstDTO));
        assertTrue(dtoList.contains(secondDTO));
    }

    @Test(expected = AttributeNotFoundException.class)
    public void whenFindAllAttributeTranslations_thenThrowAttributeNotFoundException() {
        Long attributeId = 1L;

        doReturn(Optional.empty()).when(attributeRepository).findById(attributeId);

        attributeService.findAllAttributeTranslations(attributeId);
    }

    @Test
    public void whenCreateTranslation_thenTranslationCreated() {
        Long attributeId = 1L;
        Attribute attribute = mock(Attribute.class);
        Language language = mock(Language.class);
        List<AttributeTranslation> translations = new ArrayList<>();
        AttributeTranslationDTO createDTO = new AttributeTranslationDTO();
        AttributeTranslationDTO expectedDTO = new AttributeTranslationDTO();
        createDTO.setLabel("Test");
        createDTO.setLocale("pl_PL");

        doReturn(Optional.of(attribute)).when(attributeRepository).findById(attributeId);
        doReturn(Optional.of(language)).when(languageRepository).findByLocale(createDTO.getLocale());
        doReturn(false).when(attribute).hasTranslationInLanguage(language);
        doReturn(translations).when(attribute).getAttributeTranslations();
        doReturn(expectedDTO).when(modelMapper).map(any(AttributeTranslation.class), eq(AttributeTranslationDTO.class));

        AttributeTranslationDTO createdDTO = attributeService.createTranslation(createDTO, attributeId);

        assertEquals(expectedDTO, createdDTO);
        verify(attributeRepository, times(1)).findById(attributeId);
        verify(languageRepository, times(1)).findByLocale(createDTO.getLocale());
        verify(attribute, times(1)).hasTranslationInLanguage(language);

        ArgumentCaptor<AttributeTranslation> translationCaptor = ArgumentCaptor.forClass(AttributeTranslation.class);
        verify(modelMapper, times(1)).map(translationCaptor.capture(), any());

        List<AttributeTranslation> capturedTranslation = translationCaptor.getAllValues();
        assertEquals(createDTO.getLabel(), capturedTranslation.get(0).getLabel());
        assertEquals(attribute, capturedTranslation.get(0).getAttribute());
        assertEquals(language, capturedTranslation.get(0).getLanguage());
    }

    @Test(expected = AttributeNotFoundException.class)
    public void whenCreateTranslation_thenThrowAttributeNotFoundException() {
        Long attributeId = 1L;
        AttributeTranslationDTO createDTO = new AttributeTranslationDTO();

        doReturn(Optional.empty()).when(attributeRepository).findById(attributeId);

        attributeService.createTranslation(createDTO, attributeId);
    }

    @Test(expected = LanguageNotFoundException.class)
    public void whenCreateTranslation_thenThrowLanguageNotFoundException() {
        Long attributeId = 1L;
        Attribute attribute = mock(Attribute.class);
        AttributeTranslationDTO createDTO = new AttributeTranslationDTO();

        doReturn(Optional.of(attribute)).when(attributeRepository).findById(attributeId);
        doReturn(Optional.empty()).when(languageRepository).findByLocale(createDTO.getLocale());

        attributeService.createTranslation(createDTO, attributeId);
    }

    @Test(expected = DuplicateAttributeTranslationException.class)
    public void whenCreateTranslation_thenThrowDuplicateAttributeTranslationException() {
        Long attributeId = 1L;
        Attribute attribute = mock(Attribute.class);
        Language language = mock(Language.class);
        AttributeTranslationDTO createDTO = new AttributeTranslationDTO();

        doReturn(Optional.of(attribute)).when(attributeRepository).findById(attributeId);
        doReturn(Optional.of(language)).when(languageRepository).findByLocale(createDTO.getLocale());
        doReturn(true).when(attribute).hasTranslationInLanguage(language);

        attributeService.createTranslation(createDTO, attributeId);
    }

    @Test
    public void whenUpdateTranslation_thenTranslationUpdated() {
        Long attributeId = 1L;
        String locale = "pl_PL";
        Attribute attribute = mock(Attribute.class);
        AttributeTranslation translation = mock(AttributeTranslation.class);
        UpdateAttributeTranslationDTO updateDTO = new UpdateAttributeTranslationDTO();
        AttributeTranslationDTO updatedDTO = new AttributeTranslationDTO();
        updateDTO.setLabel("newLabel");

        doReturn(Optional.of(attribute)).when(attributeRepository).findById(attributeId);
        doReturn(Optional.of(translation)).when(attribute).getTranslationForLocale(locale);
        doReturn(updatedDTO).when(modelMapper).map(translation, AttributeTranslationDTO.class);

        AttributeTranslationDTO dto = attributeService.updateTranslation(updateDTO, attributeId, locale);

        verify(attributeRepository, times(1)).findById(attributeId);
        verify(attribute, times(1)).getTranslationForLocale(locale);
        verify(translation).setLabel(updateDTO.getLabel());
        verify(modelMapper, times(1)).map(translation, AttributeTranslationDTO.class);
        assertEquals(updatedDTO, dto);
    }

    @Test(expected = AttributeNotFoundException.class)
    public void whenUpdateTranslation_thenThrowAttributeNotFoundException() {
        Long attributeId = 1L;
        String locale = "pl_PL";
        UpdateAttributeTranslationDTO updateDTO = new UpdateAttributeTranslationDTO();

        doReturn(Optional.empty()).when(attributeRepository).findById(attributeId);

        attributeService.updateTranslation(updateDTO, attributeId, locale);
    }

    @Test(expected = AttributeTranslationNotFoundException.class)
    public void whenUpdateTranslation_thenThrowAttributeTranslationNotFoundException() {
        Long attributeId = 1L;
        String locale = "pl_PL";
        Attribute attribute = mock(Attribute.class);
        UpdateAttributeTranslationDTO updateDTO = new UpdateAttributeTranslationDTO();

        doReturn(Optional.of(attribute)).when(attributeRepository).findById(attributeId);
        doReturn(Optional.empty()).when(attribute).getTranslationForLocale(locale);

        attributeService.updateTranslation(updateDTO, attributeId, locale);
    }

    @Test
    public void whenDeleteTranslation_thenDeleteTranslation() {
        Long attributeId = 1L;
        String locale = "pl_PL";

        attributeService.deleteTranslation(attributeId, locale);
        verify(attributeTranslationRepository, times(1)).deleteByAttributeIdAndLanguageLocale(attributeId, locale);
    }
}