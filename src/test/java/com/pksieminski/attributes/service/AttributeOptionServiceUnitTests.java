package com.pksieminski.attributes.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.pksieminski.attributes.repository.OptionRepository;
import com.pksieminski.attributes.dto.model.AttributeOptionDTO;
import com.pksieminski.attributes.dto.model.RelateAttributeOptionDTO;
import com.pksieminski.attributes.entity.Attribute;
import com.pksieminski.attributes.entity.AttributeOption;
import com.pksieminski.attributes.entity.AttributeOptionId;
import com.pksieminski.attributes.entity.Option;
import com.pksieminski.attributes.exception.AttributeNotFoundException;
import com.pksieminski.attributes.exception.OptionNotFoundException;
import com.pksieminski.attributes.repository.AttributeOptionRepository;
import com.pksieminski.attributes.repository.AttributeRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("tests")
@RunWith(MockitoJUnitRunner.class)
public class AttributeOptionServiceUnitTests {

    @InjectMocks
    private AttributeOptionService attributeOptionService;

    @Mock
    private AttributeRepository attributeRepository;

    @Mock
    private OptionRepository optionRepository;

    @Mock
    private AttributeOptionRepository attributeOptionRepository;

    @Mock
    private ModelMapper modelMapper;

    @Test
    public void whenRelateAttributeWithOption_thenRelate() {
        Integer sortOrder = 5;
        RelateAttributeOptionDTO relateDTO = new RelateAttributeOptionDTO();
        relateDTO.setSortOrder(sortOrder);
        Long attributeId = 1L;
        Long optionId = 2L;
        Attribute attribute = mock(Attribute.class);
        Option option = mock(Option.class);
        AttributeOptionId id = new AttributeOptionId(attributeId, optionId);
        List<AttributeOption> attributeOptions = new ArrayList<>();
        AttributeOptionDTO expectedDTO = new AttributeOptionDTO();

        doReturn(Optional.of(attribute)).when(attributeRepository).findById(attributeId);
        doReturn(Optional.of(option)).when(optionRepository).findById(optionId);
        doReturn(Optional.empty()).when(attributeOptionRepository).findById(id);
        doReturn(attributeOptions).when(option).getAttributeOptions();
        doReturn(expectedDTO).when(modelMapper).map(any(AttributeOption.class), eq(AttributeOptionDTO.class));

        AttributeOptionDTO dto = attributeOptionService.relateAttributeWithOption(relateDTO, attributeId, optionId);

        ArgumentCaptor<AttributeOption> captor = ArgumentCaptor.forClass(AttributeOption.class);
        verify(attributeRepository, times(1)).findById(attributeId);
        verify(optionRepository, times(1)).findById(optionId);
        verify(attributeOptionRepository, times(1)).findById(id);
        verify(modelMapper, times(1)).map(captor.capture(), eq(AttributeOptionDTO.class));

        assertEquals(expectedDTO, dto);
        assertTrue(attributeOptions.contains(captor.getValue()));
        assertEquals(captor.getValue().getAttribute(), attribute);
        assertEquals(captor.getValue().getOption(), option);
        assertEquals(captor.getValue().getSortOrder(), sortOrder);
    }

    @Test
    public void givenRelationshipExists_whenRelateAttributeWithOption_thenUpdate() {
        Integer sortOrder = 5;
        RelateAttributeOptionDTO relateDTO = new RelateAttributeOptionDTO();
        relateDTO.setSortOrder(sortOrder);
        Long attributeId = 1L;
        Long optionId = 2L;
        Attribute attribute = mock(Attribute.class);
        Option option = mock(Option.class);
        AttributeOptionId id = new AttributeOptionId(attributeId, optionId);
        AttributeOption attributeOption = mock(AttributeOption.class);
        AttributeOptionDTO expectedDTO = new AttributeOptionDTO();

        doReturn(Optional.of(attribute)).when(attributeRepository).findById(attributeId);
        doReturn(Optional.of(option)).when(optionRepository).findById(optionId);
        doReturn(Optional.of(attributeOption)).when(attributeOptionRepository).findById(id);
        doReturn(expectedDTO).when(modelMapper).map(any(AttributeOption.class), eq(AttributeOptionDTO.class));

        AttributeOptionDTO dto = attributeOptionService.relateAttributeWithOption(relateDTO, attributeId, optionId);

        ArgumentCaptor<AttributeOption> captor = ArgumentCaptor.forClass(AttributeOption.class);
        verify(attributeRepository, times(1)).findById(attributeId);
        verify(optionRepository, times(1)).findById(optionId);
        verify(attributeOptionRepository, times(1)).findById(id);
        verify(modelMapper, times(1)).map(captor.capture(), eq(AttributeOptionDTO.class));

        assertEquals(expectedDTO, dto);
        assertEquals(captor.getValue(), attributeOption);
    }

    @Test(expected = OptionNotFoundException.class)
    public void givenAttributeNotExists_whenRelateAttributeWithOption_thenThrowAttributeNotFoundException() {
        Long attributeId = 1L;
        Long optionId = 2L;

        Attribute attribute = mock(Attribute.class);
        RelateAttributeOptionDTO relateDTO = new RelateAttributeOptionDTO();

        doReturn(Optional.of(attribute)).when(attributeRepository).findById(attributeId);
        doReturn(Optional.empty()).when(optionRepository).findById(optionId);

        attributeOptionService.relateAttributeWithOption(relateDTO, attributeId, optionId);
    }

    @Test(expected = AttributeNotFoundException.class)
    public void givenOptionNotExists_whenRelateAttributeWithOption_thenThrowOptionNotFoundException() {
        Long attributeId = 1L;
        Long optionId = 2L;
        RelateAttributeOptionDTO relateDTO = new RelateAttributeOptionDTO();

        doReturn(Optional.empty()).when(attributeRepository).findById(attributeId);

        attributeOptionService.relateAttributeWithOption(relateDTO, attributeId, optionId);
    }

    @Test()
    public void whenUnrelateAttributeWithOption_thenUnrelate() {
        Long attributeId = 1L;
        Long optionId = 2L;

        attributeOptionService.unrelateAttributeWithOption(attributeId, optionId);

        ArgumentCaptor<AttributeOptionId> captor = ArgumentCaptor.forClass(AttributeOptionId.class);
        verify(attributeOptionRepository, times(1)).deleteById(captor.capture());

        assertEquals(attributeId, captor.getValue().getAttributeId());
        assertEquals(optionId, captor.getValue().getOptionId());
    }
}