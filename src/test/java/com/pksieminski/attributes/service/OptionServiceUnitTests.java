package com.pksieminski.attributes.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.pksieminski.attributes.dto.model.OptionDTO;
import com.pksieminski.attributes.dto.model.CreateOptionDTO;
import com.pksieminski.attributes.entity.Option;
import com.pksieminski.attributes.exception.OptionNotFoundException;
import com.pksieminski.attributes.exception.DuplicateOptionException;
import com.pksieminski.attributes.repository.OptionRepository;
import com.pksieminski.attributes.repository.LanguageRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("tests")
@RunWith(MockitoJUnitRunner.class)
public class OptionServiceUnitTests {

    @InjectMocks
    private OptionService optionService;

    @Mock
    private OptionRepository optionRepository;

    @Mock
    private LanguageRepository languageRepository;

    @Mock
    private ModelMapper modelMapper;
    
    @Test
    public void whenfindAll_thenReturnAllAsDTO() {
        Option firstOption = new Option("green");
        Option secondOption = new Option("blue");
        List<Option> list = new ArrayList<>();
        list.add(firstOption);
        list.add(secondOption);

        OptionDTO firstDTO = new OptionDTO();
        OptionDTO secondDTO = new OptionDTO();

        doReturn(list).when(optionRepository).findAll();
        doReturn(firstDTO).when(modelMapper).map(firstOption, OptionDTO.class);
        doReturn(secondDTO).when(modelMapper).map(secondOption, OptionDTO.class);
        
        List<OptionDTO> dtoList = optionService.findAll();

        verify(optionRepository, times(1)).findAll();
        verify(modelMapper, times(1)).map(firstOption, OptionDTO.class);
        verify(modelMapper, times(1)).map(secondOption, OptionDTO.class);

        assertEquals(2, dtoList.size());
        assertTrue(dtoList.contains(firstDTO));
        assertTrue(dtoList.contains(secondDTO));
    }

    @Test
    public void whenFindById_thenReturnDTO() {
        Option option = new Option("green");
        option.setId(1L);
        Optional<Option> optional = Optional.of(option);
        OptionDTO expectedDTO = new OptionDTO();

        doReturn(optional).when(optionRepository).findById(option.getId());
        doReturn(expectedDTO).when(modelMapper).map(option, OptionDTO.class);

        OptionDTO dto = optionService.findById(option.getId());

        verify(optionRepository, times(1)).findById(option.getId());
        verify(modelMapper, times(1)).map(option, OptionDTO.class);

        assertEquals(expectedDTO, dto);
    }

    @Test(expected = OptionNotFoundException.class)
    public void whenFindById_thenThrowException() {
        Long optionId = 1L;

        doReturn(Optional.empty()).when(optionRepository).findById(optionId);

        optionService.findById(optionId);
    }

    @Test
    public void whenCreate_thenCreated() {
        CreateOptionDTO createDTO = new CreateOptionDTO();
        createDTO.setCode("green");
        Option option = new Option(createDTO.getCode());
        OptionDTO expectedDTO = new OptionDTO();

        doReturn(false).when(optionRepository).existsByCode(createDTO.getCode());
        doReturn(option).when(modelMapper).map(createDTO, Option.class);
        doReturn(expectedDTO).when(modelMapper).map(option, OptionDTO.class);

        OptionDTO dto = optionService.create(createDTO);

        verify(optionRepository, times(1)).existsByCode(createDTO.getCode());
        verify(modelMapper, times(1)).map(createDTO, Option.class);
        verify(optionRepository, times(1)).save(option);
        verify(modelMapper, times(1)).map(option, OptionDTO.class);

        assertEquals(expectedDTO, dto);
    }

    @Test(expected = DuplicateOptionException.class)
    public void whenCreate_thenThrowException() {
        CreateOptionDTO createDTO = new CreateOptionDTO();

        doReturn(true).when(optionRepository).existsByCode(createDTO.getCode());

        optionService.create(createDTO);
    }

    @Test()
    public void whenDeleteById_thenDelete() {
        Long id = 1L;

        optionService.deleteById(id);

        verify(optionRepository, times(1)).deleteById(id);
    }
}