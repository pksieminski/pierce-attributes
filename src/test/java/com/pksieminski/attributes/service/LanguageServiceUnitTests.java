package com.pksieminski.attributes.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Map;

import com.pksieminski.attributes.entity.Language;
import com.pksieminski.attributes.repository.LanguageRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("tests")
@RunWith(MockitoJUnitRunner.class)
public class LanguageServiceUnitTests {

    @InjectMocks
    private LanguageService languageService;

    @Mock
    private LanguageRepository languageRepository;
    
    @Test
    public void whenFindAllMapByLocale_thenReturnAllMappedByLocale() {
        Language firstLanguage = new Language("pl_PL");
        Language secondLanguage = new Language("en_GB");

        Map<String, Language> expectedMap = new HashMap<>();
        expectedMap.put(firstLanguage.getLocale(), firstLanguage);
        expectedMap.put(secondLanguage.getLocale(), secondLanguage);

        doReturn(expectedMap).when(languageRepository).findAllMapByLocale();
        
        Map<String, Language> map = languageService.findAllMapByLocale();

        verify(languageRepository).findAllMapByLocale();

        assertEquals(expectedMap, map);
    }
}