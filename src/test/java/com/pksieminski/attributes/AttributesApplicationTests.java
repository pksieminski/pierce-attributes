package com.pksieminski.attributes;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@ActiveProfiles("tests")
@RunWith(SpringRunner.class)
public class AttributesApplicationTests {

	@Test
	public void contextLoads() {
	}

}
