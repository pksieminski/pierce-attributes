package com.pksieminski.attributes.controller;

import com.pksieminski.attributes.AttributesApplication;
import com.pksieminski.attributes.entity.Option;
import com.pksieminski.attributes.entity.Attribute;
import com.pksieminski.attributes.entity.AttributeOption;
import com.pksieminski.attributes.entity.AttributeOptionId;
import com.pksieminski.attributes.repository.OptionRepository;
import com.pksieminski.attributes.repository.AttributeOptionRepository;
import com.pksieminski.attributes.repository.AttributeRepository;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@AutoConfigureMockMvc
@ActiveProfiles("tests")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AttributesApplication.class)
public class AttributeOptionControllerIntegrationTests {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private AttributeRepository attributeRepository;

    @Autowired
    private OptionRepository optionRepository;

    @Autowired
    private AttributeOptionRepository attributeOptionRepository;

    @Before
    public void tearDown() {
        attributeOptionRepository.deleteAll();
        attributeRepository.deleteAll();
        optionRepository.deleteAll();
    }
    
    @Test
    public void whenRelateAttributeWithOption_thenRelate() throws Exception {
        Integer sortOrder = 5;
        Option option = new Option("L");
        Attribute attribute = new Attribute("size");

        attributeRepository.save(attribute);
        optionRepository.save(option);

        String json = new JSONObject().put("sortOrder", sortOrder).toString();

        mvc.perform(put("/attributes/{id}/options/{optionId}", attribute.getId(), option.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(json))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id", is(option.getId().intValue())))
            .andExpect(jsonPath("$.code", is(option.getCode())))
            .andExpect(jsonPath("$.sortOrder", is(sortOrder)))
            .andExpect(jsonPath("$.translations", hasSize(0)));
    }

    @Test
    public void givenAttributeNotExists_whenRelateAttributeWithOption_thenNotFound() throws Exception {
        String json = new JSONObject().put("sortOrder", 5).toString();
        Option option = new Option("L");
        optionRepository.save(option);

        mvc.perform(put("/attributes/{id}/options/{optionId}", 9, option.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(json))
            .andExpect(status().isNotFound())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.status", is(HttpStatus.NOT_FOUND.value())))
            .andExpect(jsonPath("$.error", is(HttpStatus.NOT_FOUND.getReasonPhrase())));
    }

    @Test
    public void givenOptionNotExists_whenRelateAttributeWithOption_thenNotFound() throws Exception {
        String json = new JSONObject().put("sortOrder", 5).toString();
        Attribute attribute = new Attribute("size");
        attributeRepository.save(attribute);

        mvc.perform(put("/attributes/{id}/options/{optionId}", attribute.getId(), 9)
            .contentType(MediaType.APPLICATION_JSON)
            .content(json))
            .andExpect(status().isNotFound())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.status", is(HttpStatus.NOT_FOUND.value())))
            .andExpect(jsonPath("$.error", is(HttpStatus.NOT_FOUND.getReasonPhrase())));
    }

    @Test
    public void givenSortOrderMissing_whenRelateAttributeWithOption_thenNotFound() throws Exception {
        Option option = new Option("L");
        Attribute attribute = new Attribute("size");

        attributeRepository.save(attribute);
        optionRepository.save(option);

        mvc.perform(put("/attributes/{id}/options/{optionId}", attribute.getId(), option.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content("{}"))
            .andExpect(status().isBadRequest())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.status", is(HttpStatus.BAD_REQUEST.value())))
            .andExpect(jsonPath("$.error", is(HttpStatus.BAD_REQUEST.getReasonPhrase())));
    }

    @Test
    public void whenUnrelateAttributeWithOption_thenDeleteRelation() throws Exception {
        Option option = new Option("L");
        Attribute attribute = new Attribute("size");
        AttributeOption attributeOption = new AttributeOption(attribute, option, 5);
        attribute.getAttributeOptions().add(attributeOption);

        attributeRepository.save(attribute);
        optionRepository.save(option);

        mvc.perform(delete("/attributes/{id}/options/{optionId}", attribute.getId(), option.getId())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());

        assertEquals(
            false, 
            attributeOptionRepository
                .findById(
                    new AttributeOptionId(attribute.getId(), option.getId())
                )
                .isPresent()
        );
    }
}