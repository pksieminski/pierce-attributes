package com.pksieminski.attributes.controller;

import com.pksieminski.attributes.AttributesApplication;
import com.pksieminski.attributes.entity.Option;
import com.pksieminski.attributes.entity.OptionTranslation;
import com.pksieminski.attributes.entity.Language;
import com.pksieminski.attributes.repository.OptionRepository;
import com.pksieminski.attributes.repository.LanguageRepository;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@AutoConfigureMockMvc
@ActiveProfiles("tests")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AttributesApplication.class)
public class OptionControllerIntegrationTests {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private OptionRepository optionRepository;

    @Autowired
    private LanguageRepository languageRepository;

    @Before
    public void tearDown() {
        optionRepository.deleteAll();
        languageRepository.deleteAll();
    }
    
    @Test
    public void whenGetOptions_thenReturnAllOptions() throws Exception {
        Language language = new Language("en_GB");
        Option firstOption = new Option("XL");
        Option secondOption = new Option("M");
        OptionTranslation translation = new OptionTranslation(firstOption, language, "XL");
        firstOption.getOptionTranslations().add(translation);
        optionRepository.save(firstOption);
        optionRepository.save(secondOption);

        mvc.perform(get("/options").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$[0].code", is(firstOption.getCode())))
            .andExpect(jsonPath("$[0].id", is(firstOption.getId().intValue())))
            .andExpect(jsonPath("$[0].translations", hasSize(1)))
            .andExpect(jsonPath("$[0].translations[0].locale", is(translation.getLanguage().getLocale())))
            .andExpect(jsonPath("$[0].translations[0].label", is(translation.getLabel())))
            .andExpect(jsonPath("$[1].code", is(secondOption.getCode())))
            .andExpect(jsonPath("$[1].id", is(secondOption.getId().intValue())))
            .andExpect(jsonPath("$[1].translations", hasSize(0)));
    }

    @Test
    public void whenGetOption_thenReturnOption() throws Exception {
        Language language = new Language("en_US");
        Option option = new Option("XL");
        OptionTranslation translation = new OptionTranslation(option, language, "XL");
        option.getOptionTranslations().add(translation);
        optionRepository.save(option);

        mvc.perform(get("/options/{id}", option.getId()).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.code", is(option.getCode())))
            .andExpect(jsonPath("$.id", is(option.getId().intValue())))
            .andExpect(jsonPath("$.translations", hasSize(1)))
            .andExpect(jsonPath("$.translations[0].locale", is(translation.getLanguage().getLocale())))
            .andExpect(jsonPath("$.translations[0].label", is(translation.getLabel())));
    }

    @Test
    public void whenGetOptionNotExists_thenNotFound() throws Exception {
        mvc.perform(get("/options/{id}", 9L).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.status", is(HttpStatus.NOT_FOUND.value())))
            .andExpect(jsonPath("$.error", is(HttpStatus.NOT_FOUND.getReasonPhrase())));
    }

    @Test
    public void whenCreateOption_thenCreated() throws Exception {
        Language language = new Language("pl_PL");
        languageRepository.save(language);

        JSONArray translations = new JSONArray()
            .put(new JSONObject()
                .put("locale", language.getLocale())
                .put("label", "XL")
            );
        JSONObject json = new JSONObject()
            .put("code", "XL")
            .put("translations", translations);

        mvc.perform(post("/options")
            .contentType(MediaType.APPLICATION_JSON)
            .content(json.toString()))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.code", is("XL")))
            .andExpect(jsonPath("$.id", greaterThan(0)))
            .andExpect(jsonPath("$.translations", hasSize(1)))
            .andExpect(jsonPath("$.translations[0].locale", is(language.getLocale())))
            .andExpect(jsonPath("$.translations[0].label", is("XL")));
    }

    @Test
    public void whenCreateOptionEmptyCode_thenBadRequest() throws Exception {
        mvc.perform(post("/options")
            .contentType(MediaType.APPLICATION_JSON)
            .content("{}"))
            .andExpect(status().isBadRequest())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.status", is(HttpStatus.BAD_REQUEST.value())))
            .andExpect(jsonPath("$.error", is(HttpStatus.BAD_REQUEST.getReasonPhrase())));
    }

    @Test
    public void whenCreateOptionDuplicate_thenBadRequest() throws Exception {
        Option option = new Option("green");
        optionRepository.save(option);

        JSONObject json = new JSONObject()
            .put("green", option.getCode());

        mvc.perform(post("/options")
            .contentType(MediaType.APPLICATION_JSON)
            .content(json.toString()))
            .andExpect(status().isBadRequest())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.status", is(HttpStatus.BAD_REQUEST.value())))
            .andExpect(jsonPath("$.error", is(HttpStatus.BAD_REQUEST.getReasonPhrase())));
    }

    @Test
    public void whenDeleteOption_thenDelete() throws Exception {
        Option option = new Option("green");
        optionRepository.save(option);

        mvc.perform(delete("/options/{id}", option.getId())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());

        assertEquals(false, optionRepository.findById(option.getId()).isPresent());
    }
}