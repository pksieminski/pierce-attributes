package com.pksieminski.attributes.controller;

import com.pksieminski.attributes.AttributesApplication;
import com.pksieminski.attributes.entity.Attribute;
import com.pksieminski.attributes.entity.AttributeOption;
import com.pksieminski.attributes.entity.AttributeTranslation;
import com.pksieminski.attributes.entity.Language;
import com.pksieminski.attributes.entity.Option;
import com.pksieminski.attributes.entity.OptionTranslation;
import com.pksieminski.attributes.repository.AttributeRepository;
import com.pksieminski.attributes.repository.AttributeTranslationRepository;
import com.pksieminski.attributes.repository.LanguageRepository;
import com.pksieminski.attributes.repository.OptionRepository;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;


@AutoConfigureMockMvc
@ActiveProfiles("tests")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AttributesApplication.class)
public class AttributeControllerIntegrationTests {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private AttributeRepository attributeRepository;

    @Autowired
    private OptionRepository optionRepository;

    @Autowired
    private AttributeTranslationRepository attributeTranslationRepository;

    @Autowired
    private LanguageRepository languageRepository;

    @After
    public void tearDown() {
        attributeRepository.deleteAll();
        optionRepository.deleteAll();
        languageRepository.deleteAll();
    }
    
    @Test
    public void whenGetAttributes_thenReturnAllAttributes() throws Exception {
        Language language = new Language("en_GB");
        Attribute firstAttribute = new Attribute("code");
        Attribute secondAttribute = new Attribute("size");
        AttributeTranslation translation = new AttributeTranslation(firstAttribute, language, "Kod");
        firstAttribute.getAttributeTranslations().add(translation);
        attributeRepository.save(firstAttribute);
        attributeRepository.save(secondAttribute);

        mvc.perform(get("/attributes").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$[0].code", is(firstAttribute.getCode())))
            .andExpect(jsonPath("$[0].id", is(firstAttribute.getId().intValue())))
            .andExpect(jsonPath("$[0].translations", hasSize(1)))
            .andExpect(jsonPath("$[0].translations[0].locale", is(translation.getLanguage().getLocale())))
            .andExpect(jsonPath("$[0].translations[0].label", is(translation.getLabel())))
            .andExpect(jsonPath("$[1].code", is(secondAttribute.getCode())))
            .andExpect(jsonPath("$[1].id", is(secondAttribute.getId().intValue())))
            .andExpect(jsonPath("$[1].translations", hasSize(0)));
    }

    @Test
    public void whenGetAttribute_thenReturnAttribute() throws Exception {
        Language language = new Language("en_US");
        Attribute attribute = new Attribute("code");
        AttributeTranslation translation = new AttributeTranslation(attribute, language, "Kod");
        attribute.getAttributeTranslations().add(translation);
        attributeRepository.save(attribute);

        mvc.perform(get("/attributes/{id}", attribute.getId()).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.code", is(attribute.getCode())))
            .andExpect(jsonPath("$.id", is(attribute.getId().intValue())))
            .andExpect(jsonPath("$.translations", hasSize(1)))
            .andExpect(jsonPath("$.translations[0].locale", is(translation.getLanguage().getLocale())))
            .andExpect(jsonPath("$.translations[0].label", is(translation.getLabel())));
    }

    @Test
    public void whenGetAttributeNotExists_thenNotFound() throws Exception {
        mvc.perform(get("/attributes/{id}", 9L).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.status", is(HttpStatus.NOT_FOUND.value())))
            .andExpect(jsonPath("$.error", is(HttpStatus.NOT_FOUND.getReasonPhrase())));
    }

    @Test
    public void whenGetAttributeDetailed_thenReturnDetailedAttribute() throws Exception {
        Language language = new Language("en_US");
        Attribute attribute = new Attribute("code");
        AttributeTranslation translation = new AttributeTranslation(attribute, language, "Kod");
        attribute.getAttributeTranslations().add(translation);
        Option option = new Option("green");
        OptionTranslation optionTranslation = new OptionTranslation(option, language, "blue");
        option.getOptionTranslations().add(optionTranslation);
        AttributeOption attributeOption = new AttributeOption(attribute, option, 5);
        attribute.getAttributeOptions().add(attributeOption);
        attributeRepository.save(attribute);

        mvc.perform(get("/attributes/{id}/detailed", attribute.getId()).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.code", is(attribute.getCode())))
            .andExpect(jsonPath("$.id", is(attribute.getId().intValue())))
            .andExpect(jsonPath("$.translations", hasSize(1)))
            .andExpect(jsonPath("$.translations[0].locale", is(translation.getLanguage().getLocale())))
            .andExpect(jsonPath("$.translations[0].label", is(translation.getLabel())))
            .andExpect(jsonPath("$.options", hasSize(1)))
            .andExpect(jsonPath("$.options[0].id", is(option.getId().intValue())))
            .andExpect(jsonPath("$.options[0].code", is(option.getCode())))
            .andExpect(jsonPath("$.options[0].sortOrder", is(attributeOption.getSortOrder())))
            .andExpect(jsonPath("$.options[0].translations", hasSize(1)))
            .andExpect(jsonPath("$.options[0].translations[0].locale", is(optionTranslation.getLanguage().getLocale())))
            .andExpect(jsonPath("$.options[0].translations[0].label", is(optionTranslation.getLabel())));
    }

    @Test
    public void whenCreateAttribute_thenCreated() throws Exception {
        Language language = new Language("pl_PL");
        languageRepository.save(language);

        JSONArray translations = new JSONArray()
            .put(new JSONObject()
                .put("locale", language.getLocale())
                .put("label", "Rozmiar")
            );
        JSONObject json = new JSONObject()
            .put("code", "size")
            .put("translations", translations);

        mvc.perform(post("/attributes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(json.toString()))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.code", is("size")))
            .andExpect(jsonPath("$.id", greaterThan(0)))
            .andExpect(jsonPath("$.translations", hasSize(1)))
            .andExpect(jsonPath("$.translations[0].locale", is(language.getLocale())))
            .andExpect(jsonPath("$.translations[0].label", is("Rozmiar")));
    }

    @Test
    public void whenCreateAttributeEmptyCode_thenBadRequest() throws Exception {
        mvc.perform(post("/attributes")
            .contentType(MediaType.APPLICATION_JSON)
            .content("{}"))
            .andExpect(status().isBadRequest())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.status", is(HttpStatus.BAD_REQUEST.value())))
            .andExpect(jsonPath("$.error", is(HttpStatus.BAD_REQUEST.getReasonPhrase())));
    }

    @Test
    public void whenCreateAttributeDuplicate_thenBadRequest() throws Exception {
        Attribute attribute = new Attribute("code");
        attributeRepository.save(attribute);

        JSONObject json = new JSONObject()
            .put("code", attribute.getCode());

        mvc.perform(post("/attributes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(json.toString()))
            .andExpect(status().isBadRequest())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.status", is(HttpStatus.BAD_REQUEST.value())))
            .andExpect(jsonPath("$.error", is(HttpStatus.BAD_REQUEST.getReasonPhrase())));
    }

    @Test
    public void whenDeleteAttribute_thenDelete() throws Exception {
        Attribute attribute = new Attribute("code");
        attributeRepository.save(attribute);

        mvc.perform(delete("/attributes/{id}", attribute.getId())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());

        assertEquals(false, attributeRepository.findById(attribute.getId()).isPresent());
    }

    @Test
    public void getAttributeTranslations_thenReturnTranslations() throws Exception {
        Language language = new Language("en_US");
        Attribute attribute = new Attribute("code");
        AttributeTranslation translation = new AttributeTranslation(attribute, language, "Kod");
        attribute.getAttributeTranslations().add(translation);
        attributeRepository.save(attribute);

        mvc.perform(get("/attributes/{id}/translations", attribute.getId()).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$[0].locale", is(translation.getLanguage().getLocale())))
            .andExpect(jsonPath("$[0].label", is(translation.getLabel())));
    }

    @Test
    public void givenAttributeNotExists_getAttributeTranslations_thenNotFound() throws Exception {
        mvc.perform(get("/attributes/{id}/translations", 9L).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.status", is(HttpStatus.NOT_FOUND.value())))
            .andExpect(jsonPath("$.error", is(HttpStatus.NOT_FOUND.getReasonPhrase())));
    }

    @Test
    public void whenCreateAttributeTranslation_thenCreated() throws Exception {
        Language language = new Language("pl_PL");
        languageRepository.save(language);
        Attribute attribute = new Attribute("code");
        attributeRepository.save(attribute);

        JSONObject json = new JSONObject()
            .put("locale", language.getLocale())
            .put("label", "Rozmiar");

        mvc.perform(post("/attributes/{id}/translations", attribute.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(json.toString()))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.locale", is(language.getLocale())))
            .andExpect(jsonPath("$.label", is("Rozmiar")));
    }

    @Test
    public void givenAttributeNotExists_whenCreateAttributeTranslation_thenNotFound() throws Exception {
        String locale = "pl_PL";
        JSONObject json = new JSONObject()
            .put("locale", locale)
            .put("label", "Rozmiar");

        mvc.perform(post("/attributes/{id}/translations", 9L)
            .contentType(MediaType.APPLICATION_JSON)
            .content(json.toString()))
            .andExpect(status().isNotFound())
            .andExpect(jsonPath("$.status", is(HttpStatus.NOT_FOUND.value())))
            .andExpect(jsonPath("$.error", is(HttpStatus.NOT_FOUND.getReasonPhrase())));
    }

    @Test
    public void updateAttributeTranslation_thenUpdate() throws Exception {
        Language language = new Language("en_US");
        Attribute attribute = new Attribute("code");
        AttributeTranslation translation = new AttributeTranslation(attribute, language, "Kod");
        attribute.getAttributeTranslations().add(translation);
        attributeRepository.save(attribute);

        String newLabel = "newLabel";
        JSONObject json = new JSONObject().put("label", newLabel);

        mvc.perform(patch("/attributes/{id}/translations/{locale}", attribute.getId(), language.getLocale())
            .content(json.toString())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.locale", is(language.getLocale())))
            .andExpect(jsonPath("$.label", is(newLabel)));
    }

    @Test
    public void givenAttributeNotExists_updateAttributeTranslation_thenNotFound() throws Exception {
        JSONObject json = new JSONObject().put("label", "newLabel");

        mvc.perform(patch("/attributes/{id}/translations/{locale}", 9L, "pl_PL")
            .content(json.toString())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound())
            .andExpect(jsonPath("$.status", is(HttpStatus.NOT_FOUND.value())))
            .andExpect(jsonPath("$.error", is(HttpStatus.NOT_FOUND.getReasonPhrase())));
    }

    @Test
    public void givenAttributeTranslationNotExists_updateAttributeTranslation_thenNotFound() throws Exception {
        Attribute attribute = new Attribute("code");
        attributeRepository.save(attribute);
        JSONObject json = new JSONObject().put("label", "newLabel");

        mvc.perform(patch("/attributes/{id}/translations/{locale}", attribute.getId(), "pl_PL")
            .content(json.toString())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound())
            .andExpect(jsonPath("$.status", is(HttpStatus.NOT_FOUND.value())))
            .andExpect(jsonPath("$.error", is(HttpStatus.NOT_FOUND.getReasonPhrase())));
    }

    @Test
    public void whenDeleteAttributeTranslation_thenDelete() throws Exception {
        Language language = new Language("en_US");
        Attribute attribute = new Attribute("code");
        AttributeTranslation translation = new AttributeTranslation(attribute, language, "Kod");
        attribute.getAttributeTranslations().add(translation);
        attributeRepository.save(attribute);

        mvc.perform(delete("/attributes/{id}/translations/{locale}", attribute.getId(), language.getLocale())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());

        assertEquals(0, attributeTranslationRepository.count());
    }
}